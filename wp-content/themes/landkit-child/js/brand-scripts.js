const config = {
  brand: {
    name: "Hawkz",
    title: "Hawkz - Estratégias Digitais",
    url: "https://www.hawkz.com.br/",
    brandsUrl: "https://www.hawkz.com.br/marca/imagens/",
    brandUrlTarget: "_blank",
    developedby: "Desenvolvido por: ",
  },
  clients: [
    {
      name: "Elo Senior",
      domain: "cursos.elosenior.com.br",
      brandVersion: "logo-hawkz-branca/logo-hawkz.png",
    },
  ],
};

window.addEventListener("DOMContentLoaded", function () {
  let brandWrapper = document.querySelector(".brand-wrapper");

  if (brandWrapper != null) {
    brandWrapper.style = "display: flex; align-items: flex-end;";

    //developby
    let brandText = document.createTextNode(config.brand.developedby);
    brandWrapper.append(brandText);

    //brand link
    let brandLink = document.createElement("a");
    brandLink.href = config.brand.url;
    brandLink.target = config.brand.brandUrlTarget;

    let clientHost = window.location.hostname;

    let client = config.clients.filter(function (client) {
      return client.domain == clientHost;
    });

    //brand image
    let img = document.createElement("img");

    if (client[0]) {
      client = client[0];

      //urlParams
      if (client.seoParams) {
        brandLink.href += "?utm_source=" + client.seoParams.utm_source;
        brandLink.href += "&utm_medium=" + client.seoParams.utm_medium;
        brandLink.href += "&utm_campaign=" + client.seoParams.utm_campaign;
      }

      img.src = config.brand.brandsUrl + client.brandVersion;
      img.alt = config.brand.title;
      img.title = config.brand.title;
      img.style = "margin-left: 7px;";
    } else {
      img.src = "https://www.hawkz.com.br/src/imagens/logo-hawkz.png";
      img.alt = "Hawkz - Estratégias Digitais";
      img.title = "Hawkz - Estratégias Digitais";
    }

    brandLink.appendChild(img);
    brandWrapper.appendChild(brandLink);
  }
});
