<?php
/**
 * Child theme functions.
 */

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '-child/style.css' );
    wp_register_script( 'modernizr', get_stylesheet_directory_uri().'/js/modernizr-custom.js', array(), false, true);
    wp_enqueue_script( 'modernizr');

    //hawkz marca
    wp_enqueue_script( 'brand-scripts', get_stylesheet_directory_uri() . '/js/brand-scripts.min.js', Array(), false, true );
}

add_filter('use_block_editor_for_post', '__return_false');